(function () {
    'use strict';

    angular.module('main', [
        'ui.router',
        'ngMaterial',
        'main.population'
    ])

        .config(function ($locationProvider, $mdIconProvider, $mdThemingProvider) {

            $locationProvider.html5Mode({
                enabled: true,
                requireBase: false
            });

            $mdIconProvider
                .defaultIconSet("./svg/avatars.svg", 128)
                .icon("menu", "./svg/menu.svg", 24)
                .icon("share", "./svg/share.svg", 24)
                .icon("google_plus", "./svg/google_plus.svg", 24)
                .icon("hangouts", "./svg/hangouts.svg", 24)
                .icon("twitter", "./svg/twitter.svg", 24)
                .icon("phone", "./svg/phone.svg", 24);

            $mdThemingProvider.theme('default')
                .primaryPalette('brown')
                .accentPalette('red');

        });

})();