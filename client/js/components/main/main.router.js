(function () {
    'use strict';

    angular.module('main')
        .config(MainRouter);

    MainRouter.$inject = ['$stateProvider', '$urlRouterProvider'];

    function MainRouter($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('population', {
                url: '/',
                templateUrl: '/population.html'
            });

        $urlRouterProvider.otherwise('/');
    }
})();
