(function () {
    'use strict';

    angular
        .module('main')
        .controller('MainController', MainController);

    MainController.$inject = ['$mdSidenav', 'PopulationModel', 'PopulationFactory', '$rootScope', '$http'];

    function MainController($mdSidenav, PopulationModel, PopulationFactory, $rootScope, $http) {

        var vm = this;

        vm.loading = true;
        vm.model = PopulationModel;
        vm.toggleLeft = buildToggler('left');
        vm.logout = logout;

        function logout(){
           $http.get('/logout').success(function(){
               window.location = "/";
           });
        }

        function buildToggler(componentId) {
            return function () {
                $mdSidenav(componentId).toggle();
            };
        }

        vm.filters = {
            professions: {},
            friends: {}
        };

        vm.applyFilters = function () {
            PopulationFactory.setFilters(vm.filters);
            vm.toggleLeft();
        };

        $rootScope.$watch(
            function() {
                return vm.filters;
            },
            function(nValue){
                if(!vm.loading) PopulationFactory.setFilters(vm.filters);
            },
            true);

        PopulationFactory.getFilters()
            .then(function (response) {
                vm.loading = false;
            }, function () {
                vm.loading = false;
                // TODO Display error
            });

        $http.get('user').success(function(u){
            vm.user = u;
            console.log(u);
        });

    }

})();
