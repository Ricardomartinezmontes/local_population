(function () {
    'use strict';
    angular
        .module('main.population')
        .factory('PopulationFactory', PopulationFactory);

    PopulationFactory.$inject = ['$http', 'PopulationModel', '$mdToast'];

    function PopulationFactory($http, PopulationModel, $mdToast) {

        var filters = {
            professions: [],
            friends: []
        };

        return {
            getPopulation: getPopulation,
            getFilters: getFilters,
            getCount: getCount,
            setFilters: setFilters
        };

        function getPopulation() {

            var tempParams = {};
            if(filters.professions.length > 0) tempParams.professions = filters.professions.join(",");
            if(filters.friends.length > 0) tempParams.friends = filters.friends.join(",");

            return $http({
                method:'GET',
                url:'/api/population',
                params: tempParams
            })
                .then(function successQueryPopulation(response) {
                    $mdToast.show(
                        $mdToast.simple()
                            .textContent(response.data.length + ' gnomes found!')
                            .hideDelay(3000)
                    );
                    PopulationModel.Population = response.data;
                    return PopulationModel.Population;
                });
        }

        function getFilters() {
            return $http({
                method:'GET',
                url:'/api/population/filters'
            })
                .then(function successQueryPopulation(response) {
                    PopulationModel.Filters = response.data;
                    return PopulationModel.Filters;
                });
        }

        function getCount() {
            return PopulationModel.Population.length;
        }

        function setFilters(newFilters) {
            filters = {
                professions: [],
                friends: []
            };
            Object.keys(newFilters.professions).forEach(function(key){
                if(newFilters.professions[key]) filters.professions.push(key);
            });
            Object.keys(newFilters.friends).forEach(function(key){
                if(newFilters.friends[key]) filters.friends.push(key);
            });
            getPopulation();
        }

    }
})();
