(function(){
    'use strict';
    angular
        .module('main.population')
        .factory('PopulationModel',PopulationModel);

    function PopulationModel(){

        return {
            Population : [],
            Filters: []
        };

    }
})();
