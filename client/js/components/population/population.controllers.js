(function () {
    'use strict';

    angular
        .module('main.population')
        .controller('PopulationController', PopulationController);

    PopulationController.$inject = ['PopulationFactory', 'PopulationModel'];

    function PopulationController(PopulationFactory, PopulationModel) {

        var vm = this;
        vm.model = PopulationModel;

        // TODO Put loading on view
        vm.loading = true;
        PopulationFactory.getPopulation()
            .then(function successGetPopulation(response) {
                vm.loading = false;
            }, function errorGetPopulation() {
                vm.loading = false;
                // TODO Display error
            });

    }
})();
