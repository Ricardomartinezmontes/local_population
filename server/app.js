/**
 * Created by Ricardo Martinez Montes on 08/02/2017.
 */

'use strict';

var populationModel = require('./models/population');

exports.prepareApp = prepareApp;

function prepareApp(cb) {

    populationModel.start(function(err){
        if(err){
            cb(err);
        } else {
            console.log("Population Data Loaded!");
            cb(null, getApp());
        }
    });

}

function getApp(){
    var express = require('express');
    var path = require('path');
    var favicon = require('serve-favicon');
    var logger = require('morgan');
    var cookieParser = require('cookie-parser');
    var bodyParser = require('body-parser');
    var expressSession = require('express-session');
    var cors = require('cors');

    var auth = require('./auth');

    var population = require('./routes/population');

    var app = express();

    // view engine setup
    app.set('views', path.join(__dirname, 'views'));
    app.set('view engine', 'jade');

    // uncomment after placing your favicon in /public
    //app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
    app.use(logger('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(cookieParser());
    app.use(expressSession({ secret: 'population', resave: true, saveUninitialized: true }));

    auth.load(app);

    app.use(express.static(path.join(__dirname, '../build')));
    app.use(express.static(path.join(__dirname, '../node_modules')));
    app.use(express.static(path.join(__dirname, '../client/assets')));

    app.use('/api/population', population);

    // catch 404 and forward to error handler
    app.use(function(req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });

    // error handler
    app.use(function(err, req, res, next) {
        // set locals, only providing error in development
        res.locals.message = err.message;
        res.locals.error = req.app.get('env') === 'development' ? err : {};

        // render the error page
        res.status(err.status || 500);
        res.render('error');
    });

    return app;

}



