/**
 * Created by Ricardo Martínez on 22/02/2017.
 */

var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var passport = require('passport');

// TODO Verificar entorno para cambiar url de retorno
var ids = {
    facebook: {
        clientID: 'get_your_own',
        clientSecret: 'get_your_own',
        callbackURL: '/auth/facebook/callback'
    },
    google: {
        clientID: '266896111502-ctq6tmvolbku691iqm4ela295legiovb.apps.googleusercontent.com',
        clientSecret: '-w-HSxrwoiO5715mXqKEmT9F',
        callbackURL: '/auth/google/callback'
        //callbackURL: 'http://population.ricardomartinezmontes.com/auth/google/callback'
    }
};

exports.load = function (app) {
    app.use(passport.initialize());
    app.use(passport.session());

    // serialize and deserialize
    passport.serializeUser(function (user, done) {
        console.log(user.displayName + " logged!");
        done(null, user);
    });
    passport.deserializeUser(function (obj, done) {
        done(null, obj);
    });

    // GOOGLE STRATEGY
    passport.use(new GoogleStrategy({
            clientID: ids.google.clientID,
            clientSecret: ids.google.clientSecret,
            callbackURL: ids.google.callbackURL
        },
        function (request, accessToken, refreshToken, profile, done) {
            process.nextTick(function () {
                return done(null, profile);
            });
        }
    ));

    app.get('/auth/google/callback',
        passport.authenticate('google', {failureRedirect: '/'}),
        function (req, res) {
            res.redirect('/');
        });

    app.get('/auth/google',
        passport.authenticate('google', {
            scope: ['email', 'profile']
        }));

    app.use(function (req, res, next) {
        if (!req.isAuthenticated()) {
            // return res.redirect("/auth/google");
            return res.render('login');
        }
        next();
    });

    app.get('/user', function (req, res) {
        res.json(req.user._json);
    });

    app.get('/logout', function (req, res) {
        console.log(req.user.displayName + " log out!");
        req.logout();
        res.send();
        //res.redirect('/');
    });


};