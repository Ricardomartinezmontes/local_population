/**
 * Created by Ricardo Martinez Montes on 08/02/2017.
 */

'use strict';

var request = require("request");
var fs = require("fs");

module.exports.start = start;
module.exports.refreshData = refreshData;

function start(cb) {
    var self = this;

    if(!global.data) global.data = {};

    try {
        if (fs.existsSync('tmp/cache/population.json')) {
            try {
                data.population = JSON.parse(fs.readFileSync('tmp/cache/population.json', 'utf8')).Brastlewark;
                setFilters(data.population);
                cb();
            } catch (err) {
                console.log(err);
                self.refreshData(cb);
            }
        } else {
            self.refreshData(cb);
        }
    } catch (err) {
        cb(err);
    }

    setInterval(refreshData, 360000)

}

function setFilters(population) {
    data.filters = {
        professions: [],
        friends: []
    };
    population.forEach(function(pop){
        pop.professions.forEach(function(pro){
            if(pro === " Tinker") pro = "Tinker";
            if(data.filters.professions.indexOf(pro) === -1){
                data.filters.professions.push(pro);
            }
        });
        data.filters.professions.sort();
        pop.friends.forEach(function(fri){
            if(data.filters.friends.indexOf(fri) === -1){
                data.filters.friends.push(fri);
            }
        });
        data.filters.friends.sort();
    });
}

function refreshData(cb) {
    var self = this;
    try {
        request('http://www.mocky.io/v2/57fbba4c0f0000f4124fd451', function (error, response, body) {
            if (!error && response.statusCode == 200) {
                data.population = JSON.parse(body).Brastlewark;
                setFilters(data.population);
                self.lastSync = new Date();
                if(cb) cb();
                console.log(self.lastSync + " - Population data refreshed!");
                if (!fs.existsSync('tmp')) fs.mkdirSync('tmp');
                if (!fs.existsSync('tmp/cache')) fs.mkdirSync('tmp/cache');
                fs.writeFile('tmp/cache/population.json', body, 'utf8');
            } else {
                throw error;
            }
        });
    } catch (err) {
        if(cb) cb(err)
    }
}
