/**
 * Created by Ricardo Martinez Montes on 08/02/2017.
 */

var express = require('express');
var _ = require('lodash');
var router = express.Router();

router.get('/', function (req, res, next) {
    var population = [],
     friends =  req.query.friends ? req.query.friends.split(",") : [],
     professions = req.query.professions? req.query.professions.split(",") : [];

    global.data.population.forEach(function (p) {
        var professionCounter = _.intersection(p.professions, professions).length;
        var friendsCounter = _.intersection(p.friends, friends).length;
        if (friends.length > 0 && professions.length > 0 && (professionCounter === 0 || friendsCounter === 0)) return;
        if ((friends.length > 0 || professions.length > 0) && professionCounter === 0 && friendsCounter === 0) return;
        population.push(p);
    });

    res.json(population);
});

router.get('/filters', function (req, res, next) {
    res.json(data.filters);
});

module.exports = router;