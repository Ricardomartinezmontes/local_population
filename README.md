# Local population
Local population app for Brastlewark town

# Demo online

* [population.ricardomartinezmontes.com](http://population.ricardomartinezmontes.com/)

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
What things you need to install the software and how to install them
```
$ npm install gulp -g
```

## Install and run
```
$ npm install && gulp build && node main.js
```
* [http://localhost:3001](http://localhost:3001) - Default url



## Development mode
Watch code and recompile when saving
```
$ gulp
```
On the other hand starts the server
```
$ node main.js
```

## Deployment production [UNDER DEVELOPMENT]
Add additional notes about how to deploy this on a live system
```
$ gulp prod
```

## Built With

* [NodeJS](https://nodejs.org) - The server framework used
* [AngularJS](https://angularjs.org/) -  Toolset for building the framework 
* [Lodash](https://lodash.com) - A modern JavaScript utility library delivering modularity, performance & extras.
* [Passport](http://www.passportjs.org/) - Simple, unobtrusive authentication for Node.js
